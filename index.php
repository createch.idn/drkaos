<?php
$page = 'home';
include('header.php')
?>

<!-- slider-area start -->
<section class="slider-area">
    <!-- slider start -->
    <div class="slider">
        <div id="mainSlider" class="nivoSlider nevo-slider">
            <img src="img/slider/slider-1.jpg" alt="main slider" title="#htmlcaption1" />
            <img src="img/slider/slider-2.jpg" alt="main slider" title="#htmlcaption2" />
        </div>
        <div id="htmlcaption1" class="nivo-html-caption slider-caption">
            <div class="slider-progress"></div>
            <div class="slider-text">
                <div class="middle-text">
                    <div class="width-cap">
                        <h3 class="slider-tiile-top top-ani-1"><span>Mad For Summer</span></h3>
                        <h2 class="slider-tiile-middle middle-ani-1"><span>stripes</span></h2>
                        <div class="slider-readmore">
                            <a href="#">explorer</a>
                        </div>
                        <div class="slider-shopping">
                            <a href="#">Shopping Now</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="htmlcaption2" class="nivo-html-caption slider-caption">
            <div class="slider-progress"></div>
            <div class="slider-text">
                <div class="middle-text">
                    <div class="width-cap">
                        <h3 class="slider2-tiile-top top-ani-2"><span>Hand Cut and Crafted in soft Nenuine Leather</span></h3>
                        <h2 class="slider2-tiile-middle middle-ani-2"><span>Leather Bags</span></h2>
                        <div class="slider2-readmore">
                            <a href="#">explorer</a>
                        </div>
                        <div class="slider2-shop">
                            <a href="#">Shopping Now</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- slider end -->
</section>
<!-- slider-area end -->

<section class="welcome_area" style="margin-top: 50px;">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="common_heading">
                    <center>
                    <h2>WELCOME TO OUR STORE</h2>
                    
                    <h5 style="width: 50%;">Motivasi Dr. Kaos diawal adalah untuk membantu semua kalangan yang ingin menuangkan ide / karya melalui kaos dengan cepat, tanpa ribet dan tanpa harus memikirkan minimal order karena di Dr. Kaos kami siap menerima produksi mulai satuan maupun ribuan.</h5>
                    </center>
                </div>
            </div>
        </div>
    </div>
</section>

<!--Start Lateast Collection bottom area -->
<div class="banner_area">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                <div class="single_image">
                    <a href="#"><img class="banner_home1" src="img/collection-image/banner-11.jpg" alt="" /></a>
                    <div class="banner_text">
                        <h3><a href="#">Kategori 1</a></h3>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                <div class="single_image">
                    <a href="#"><img class="banner_home1" src="img/collection-image/banner-12.jpg" alt="" /></a>
                    <div class="banner_text">
                        <h3><a href="#">Kategori 2</a></h3>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                <div class="single_image">
                    <a href="#"><img class="banner_home1" src="img/collection-image/banner-13.jpg" alt="" /></a>
                    <div class="banner_text">
                        <h3><a href="#">Kategori 3</a></h3>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--End Lateast Collection bottom area -->

<!--Start Feature area -->
<div class="feature_area" style="margin-top: 100px;">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="feature_text">
                    <h4>Favorite Product</h4>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12" style="margin-bottom:30px;">
                <div class="product_list">
                    <div class="single_product">
                        <a href="detail_product.php"><img src="img/feature-image/1.jpg" alt="" /></a>
                        <div class="product_details">
                            <h2>Lorem ipsum dolor</h2>
                            <p><span class="regular_price">$170.00</span> <span class="popular_price">$150.00</span></p>
                        </div>
                        <div class="sale_product">
                            <h5>Sale</h5>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12" style="margin-bottom:30px;">
                <div class="product_list">
                    <div class="single_product">
                        <a href="detail_product.php"><img src="img/feature-image/4.jpg" alt="" /></a>
                        <div class="product_details">
                            <h2>Lorem ipsum dolor</h2>
                            <p><span class="regular_price">$170.00</span> <span class="popular_price">$150.00</span></p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12" style="margin-bottom:30px;">
                <div class="product_list">
                    <div class="single_product">
                        <a href="detail_product.php"><img src="img/feature-image/6_1.jpg" alt="" /></a>
                        <div class="product_details">
                            <h2>Lorem ipsum dolor</h2>
                            <p><span class="regular_price">$170.00</span> <span class="popular_price">$150.00</span></p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12" style="margin-bottom:30px;">
                <div class="product_list">
                    <div class="single_product">
                        <a href="detail_product.php"><img src="img/feature-image/8.jpg" alt="" /></a>
                        <div class="product_details">
                            <h2>Lorem ipsum dolor</h2>
                            <p><span class="regular_price">$170.00</span> <span class="popular_price">$150.00</span></p>
                        </div>
                        <div class="sale_product">
                            <h5>Sale</h5>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12" style="margin-bottom:30px;">
                <div class="product_list">
                    <div class="single_product">
                        <a href="detail_product.php"><img src="img/feature-image/23.jpg" alt="" /></a>
                        <div class="product_details">
                            <h2>Lorem ipsum dolor</h2>
                            <p><span class="regular_price">$170.00</span> <span class="popular_price">$150.00</span></p>
                        </div>
                        <div class="sale_product">
                            <h5>Sale</h5>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12" style="margin-bottom:30px;">
                <div class="product_list">
                    <div class="single_product">
                        <a href="detail_product.php"><img src="img/feature-image/24.jpg" alt="" /></a>
                        <div class="product_details">
                            <h2>Lorem ipsum dolor</h2>
                            <p><span class="regular_price">$170.00</span> <span class="popular_price">$150.00</span></p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12" style="margin-bottom:30px;">
                <div class="product_list">
                    <div class="single_product">
                        <a href="detail_product.php"><img src="img/feature-image/10.jpg" alt="" /></a>
                        <div class="product_details">
                            <h2>Lorem ipsum dolor</h2>
                            <p><span class="regular_price">$170.00</span> <span class="popular_price">$150.00</span></p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12" style="margin-bottom:30px;">
                <div class="product_list">
                    <div class="single_product">
                        <a href="detail_product.php"><img src="img/feature-image/11_1.jpg" alt="" /></a>
                        <div class="product_details">
                            <h2>Lorem ipsum dolor</h2>
                            <p><span class="regular_price">$170.00</span> <span class="popular_price">$150.00</span></p>
                        </div>
                        <div class="sale_product">
                            <h5>Sale</h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="more_feature_area">
                    <h2><a href="product.php">Product More...</a></h2>
                </div>
            </div>
        </div>
    </div>
</div>
<!--End Feature area -->

<!--Start Testimonila area -->
<div class="row" style="margin-top: 50px;">
    <div class="col-lg-12">
        <div class="feature_text">
            <h4>They Says</h4>
        </div>
    </div>
</div>
<div class="carousel_testimonial_area">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="testimonial_list">
                    <div class="testimonial_single">
                        <a href="#">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</a>
                        <p>alexmax</p>
                        <div class="author">
                            <img src="img/author/avatar.png" alt="" />
                        </div>
                    </div>
                    <div class="testimonial_single">
                        <a href="#">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</a>
                        <p>alexmax</p>
                        <div class="author">
                            <img src="img/author/avatar.png" alt="" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--End Testimonila area -->

<!--Start Branding area -->
<div class="branding_area">
    <div class="container">
        <div class="row">
            <div class="carousel_branding">
                <div class="col-lg-2">
                    <div class="single_branding">
                        <a href="#"><img src="img/branding-image/brand1.jpg" alt="" /></a>
                    </div>
                </div>
                <div class="col-lg-2">
                    <div class="single_branding">
                        <a href="#"><img src="img/branding-image/brand2.jpg" alt="" /></a>
                    </div>
                </div>
                <div class="col-lg-2">
                    <div class="single_branding">
                        <a href="#"><img src="img/branding-image/brand3.jpg" alt="" /></a>
                    </div>
                </div>
                <div class="col-lg-2">
                    <div class="single_branding">
                        <a href="#"><img src="img/branding-image/brand4.html" alt="" /></a>
                    </div>
                </div>
                <div class="col-lg-2">
                    <div class="single_branding">
                        <a href="#"><img src="img/branding-image/brand1.jpg" alt="" /></a>
                    </div>
                </div>
                <div class="col-lg-2">
                    <div class="single_branding">
                        <a href="#"><img src="img/branding-image/brand2.jpg" alt="" /></a>
                    </div>
                </div>
                <div class="col-lg-2">
                    <div class="single_branding">
                        <a href="#"><img src="img/branding-image/brand3.jpg" alt="" /></a>
                    </div>
                </div>
                <div class="col-lg-2">
                    <div class="single_branding">
                        <a href="#"><img src="img/branding-image/brand4.html" alt="" /></a>
                    </div>
                </div>
                <div class="col-lg-2">
                    <div class="single_branding">
                        <a href="#"><img src="img/branding-image/brand1.jpg" alt="" /></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--End Branding area -->

<!--Start Blog area -->
<div class="blog_post_area" style="margin-top: 50px; margin-bottom: 50px;">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="feature_text">
                    <h4>Our Article</h4>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="carousel_blog_list">
                <div class="col-lg-3">
                    <div class="single_blog">
                        <a href="#"><img src="https://images.unsplash.com/photo-1578682443756-573e32709f94?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1050&q=80" alt="" /></a>
                        <div class="blog_details">
                            <a href="#">William Eto</a>
                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam...</p>
                            <h3>18 Aug 2015</h3>
                            <a href="#">
                                <div class="read_more">
                                    <i class="fa fa-angle-right"></i>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="single_blog">
                        <a href="#"><img src="https://images.unsplash.com/photo-1523199455310-87b16c0eed11?ixlib=rb-1.2.1&ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&auto=format&fit=crop&w=1050&q=80" alt="" /></a>
                        <div class="blog_details">
                            <a href="#">William Eto</a>
                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam...</p>
                            <h3>18 Aug 2015</h3>
                            <a href="#">
                                <div class="read_more">
                                    <i class="fa fa-angle-right"></i>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="single_blog">
                        <a href="#"><img src="https://images.unsplash.com/photo-1523381210434-271e8be1f52b?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1050&q=80" alt="" /></a>
                        <div class="blog_details">
                            <a href="#">William Eto</a>
                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam...</p>
                            <h3>18 Aug 2015</h3>
                            <a href="#">
                                <div class="read_more">
                                    <i class="fa fa-angle-right"></i>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="single_blog">
                        <a href="#"><img src="https://images.unsplash.com/photo-1564584217132-2271feaeb3c5?ixlib=rb-1.2.1&ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&auto=format&fit=crop&w=1050&q=80" alt="" /></a>
                        <div class="blog_details">
                            <a href="#">William Eto</a>
                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam...</p>
                            <h3>18 Aug 2015</h3>
                            <a href="#">
                                <div class="read_more">
                                    <i class="fa fa-angle-right"></i>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="single_blog">
                        <a href="#"><img src="img/blog-post-image/cl-3.jpg" alt="" /></a>
                        <div class="blog_details">
                            <a href="#">William Eto</a>
                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam...</p>
                            <h3>18 Aug 2015</h3>
                            <a href="#">
                                <div class="read_more">
                                    <i class="fa fa-angle-right"></i>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="single_blog">
                        <a href="#"><img src="img/blog-post-image/cl-1.jpg" alt="" /></a>
                        <div class="blog_details">
                            <a href="#">William Eto</a>
                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam...</p>
                            <h3>18 Aug 2015</h3>
                            <a href="#">
                                <div class="read_more">
                                    <i class="fa fa-angle-right"></i>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="more_feature_area">
                    <h2><a href="article.php">Article More...</a></h2>
                </div>
            </div>
        </div>
    </div>
</div>
<!--End blog area -->

<?php
include('footer.php')
?>