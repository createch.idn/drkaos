<?php
$page = 'product';
include('header.php')
?>

<!-- Start preview Product details area -->
<div class="blog_single_view_area">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <div class="my_tabs">
                    <div class="tab-content tab_content_style">
                        <div id="tab1" class="tab-pane fade in active">
                            <div class="blog_tabs">
                                <a class="fancybox" href="img/product/blog-big-1.jpg" data-fancybox-group="gallery" title="Lorem ipsum dolor sit amet"><img src="img/product/blog-big-1.jpg" alt="" /></a>
                            </div>
                        </div>
                        <div id="tab2" class="tab-pane fade">
                            <div class="blog_tabs">
                                <a class="fancybox" href="img/product/blog-big-2.jpg" data-fancybox-group="gallery" title="Lorem ipsum dolor sit amet"><img src="img/product/blog-big-2.jpg" alt="" /></a>
                            </div>
                        </div>
                        <div id="tab3" class="tab-pane fade">
                            <div class="blog_tabs">
                                <a class="fancybox" href="img/product/blog-big-3.jpg" data-fancybox-group="gallery" title="Lorem ipsum dolor sit amet"><img src="img/product/blog-big-3.jpg" alt="" /></a>
                            </div>
                        </div>
                        <div id="tab4" class="tab-pane fade">
                            <div class="blog_tabs">
                                <a class="fancybox" href="img/product/blog-big-4.jpg" data-fancybox-group="gallery" title="Lorem ipsum dolor sit amet"><img src="img/product/blog-big-4.jpg" alt="" /></a>
                            </div>
                        </div>
                        <div id="tab5" class="tab-pane fade">
                            <div class="blog_tabs">
                                <a class="fancybox" href="img/product/blog-big-5.jpg" data-fancybox-group="gallery" title="Lorem ipsum dolor sit amet"><img src="img/product/blog-big-5.jpg" alt="" /></a>
                            </div>
                        </div>
                    </div>
                    <div class="blog_view_list">
                        <ul class="tab_style tab_bottom">
                            <li class="active">
                                <div class="blog_single_carousel">
                                    <a data-toggle="tab" href="#tab1"><img src="img/product/blog-big-1.jpg" alt="" /></a>
                                </div>
                            </li>
                            <li>
                                <div class="blog_single_carousel">
                                    <a data-toggle="tab" href="#tab2"><img src="img/product/blog-big-2.jpg" alt="" /></a>
                                </div>
                            </li>
                            <li>
                                <div class="blog_single_carousel">
                                    <a data-toggle="tab" href="#tab3"><img src="img/product/blog-big-3.jpg" alt="" /></a>
                                </div>
                            </li>
                            <li>
                                <div class="blog_single_carousel">
                                    <a data-toggle="tab" href="#tab4"><img src="img/product/blog-big-4.jpg" alt="" /></a>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <div class="blog_product_details">
                    <h2 class="blog_heading"><a href="#">Lorem ipsum dolor</a></h2>
                    <div class="pricing_rate">
                        <p class="stack">Availability:<span class="in-stock"> In stock</span></p>
                        <p class="rating_dollor rating_margin"><span class="rating_value_one dollor_size">$170.00</span> <span class="rating_value_two">$150.00</span></p>
                        <p class="blog_texts">Nunc facilisis sagittis ullamcorper. Proin lectus ipsum, gravida et mattis vulputate, tristique ut lectus. Sed et lorem nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Aenean eleifend laoreet congue. Vivamus adipiscing nisl ut dolor dignissim semper. Nulla luctus malesuada tincidunt. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Integer enim purus, posuere at ultricies eu, placerat a felis. Suspendisse aliquet urna pretium eros convallis interdum. Quisque in arcu id dui vulputate mollis eget non arcu. Aenean et nulla purus. Mauris vel tellus non nunc mattis lobortis.</p>
                    </div>
                </div>
                <div class="product_options_area">
                    <div class="product_options_selection">
                        <ul id="options_selection">
                            <li><span class="star_color">*</span><span class="Product_color">color</span> <span class="required">*Required Fields</span></li>
                            <li>
                                <select>
                                    <option value="" selected="selected">-- Please Select --</option>
                                    <option value="">black +$2.00</option>
                                    <option value="">blue +$1.00</option>
                                    <option value="">yellow +$1.00</option>
                                </select>
                            </li>
                            <li><span class="star_color">*</span><span class="Product_color">size</span></li>
                            <li>
                                <select>
                                    <option value="" selected="selected">-- Please Select --</option>
                                    <option value="">L +$2.00</option>
                                    <option value="">M +$1.00</option>
                                </select>
                            </li>
                        </ul>
                    </div>
                    <div class="cart_blog_item">
                        <div class="add-to-cart">
                            <input type="text" title="Qty" value="1" class="qty" />
                            <button type="button" title="Add to Cart" class="cart_button"><span>Add to Cart</span></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End preview Product details area -->

<!-- Start Related products area -->
<div class="related_products_area">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="feature_text feature_upsell">
                    <h4>Related Products</h4>
                </div>
                <div class="row">
                    <div class="upsell_product_list">
                        <div class="col-lg-3">
                            <div class="single_upsell">
                                <a href="#"><img src="img/product/16.jpg" alt="" /></a>
                                <div class="upsell_details">
                                    <h2><a href="#">Vivamus eu imper</a></h2>
                                    <p>$122.00</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="single_upsell">
                                <a href="#"><img src="img/product/17_2_1_1.jpg" alt="" /></a>
                                <div class="upsell_details">
                                    <h2><a href="#">Vivamus eu imper</a></h2>
                                    <p>$122.00</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="single_upsell">
                                <a href="#"><img src="img/product/18_1.jpg" alt="" /></a>
                                <div class="upsell_details">
                                    <h2><a href="#">Vivamus eu imper</a></h2>
                                    <p>$122.00</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="single_upsell">
                                <a href="#"><img src="img/product/6_1.jpg" alt="" /></a>
                                <div class="upsell_details">
                                    <h2><a href="#">Vivamus eu imper</a></h2>
                                    <p>$122.00</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="single_upsell">
                                <a href="#"><img src="img/product/13_4.jpg" alt="" /></a>
                                <div class="upsell_details">
                                    <h2><a href="#">Vivamus eu imper</a></h2>
                                    <p>$122.00</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Related products area -->

<?php
include('footer.php')
?>