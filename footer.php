<!--Start Footer area -->
<div class="footer_area home1_margin_top">
    <div class="container">
        <div class="row">
            <div class="footer_menu_area">
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="help_support">
                        <h2>ABOUT</h2>
                        <ul class="footer_menu">
                            <li><a href="#">My Account</a></li>
                            <li><a href="#">Login</a></li>
                            <li><a href="#">My Cart</a></li>
                            <li><a href="#">Wishlist</a></li>
                            <li><a href="#">Checkout</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="help_support help_border">
                        <h2>MENU</h2>
                        <ul class="footer_menu">
                            <li><a href="index.php">Home</a></li>
                            <li><a href="product.php">Product</a></li>
                            <li><a href="about.php">About Us</a></li>
                            <li><a href="article.php">Article</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">

                </div>
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 mb-5">
                    <img src="img/logo/logo.png" style="margin-top: 30%; background: white; padding: 10px; border-radius: 10px;" alt="">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="footer_bottom_area">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="copy_visa">
                        <div class="copy_right">
                            <h2>Copyright © 2021 <a href="http://createch.id" target="blank">Createch.id</a></h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--End Footer area -->
<!-- jquery JS -->
<script src="js/vendor/jquery-1.11.3.min.js"></script>
<!-- bootstrap JS -->
<script src="js/bootstrap.min.js"></script>
<!-- Mobile menu JS -->
<script src="js/jquery.meanmenu.js"></script>
<!-- jquery.easing js -->
<script src="js/jquery.easing.1.3.min.js"></script>
<!-- knob circle js -->
<script src="js/jquery.knob.js"></script>
<!-- fancybox JS -->
<script src="fancy-box/jquery.fancybox.pack.js"></script>
<!-- price slider JS  -->
<script src="js/price-slider.js"></script>
<!-- nivo slider JS -->
<script src="js/jquery.nivo.slider.pack.js"></script>
<!-- wow JS -->
<script src="js/wow.html"></script>
<!-- nivo-plugin JS -->
<script src="js/nivo-plugin.js"></script>
<!-- scrollUp JS -->
<script src="js/jquery.scrollUp.js"></script>
<!-- carousel JS -->
<script src="js/owl.carousel.min.js"></script>
<!-- plugins JS -->
<script src="js/plugins.js"></script>
<!-- main JS  -->
<script src="js/main.js"></script>
</body>

</html>