<?php
$page = 'product';
include('header.php')
?>

<!--Start clothing product area -->
<div class="clothing_product_area">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                <div class="catagory_price_color">
                    <div class="catagory_area">
                        <h2>CATEGORY</h2>
                        <ul class="catagory">
                            <li><a href="#"><i class="fa fa-angle-right"></i>LEARNING</a> <span>(4)</span></li>
                            <li><a href="#"><i class="fa fa-angle-right"></i>LIGHTING</a><span>(6)</span></li>
                            <li><a href="#"><i class="fa fa-angle-right"></i>LIVING ROOMS</a><span>(8)</span></li>
                            <li><a href="#"><i class="fa fa-angle-right"></i>LAMP</a><span>(10)</span></li>
                        </ul>
                    </div>
                    <div class="catagory_area">
                        <h2>COLOR</h2>
                        <ul class="catagory">
                            <li><a href="#"><i class="fa fa-angle-right"></i>BLACK</a> <span>(1)</span></li>
                            <li><a href="#"><i class="fa fa-angle-right"></i>BLUE</a><span>(2)</span></li>
                            <li><a href="#"><i class="fa fa-angle-right"></i>GREEN</a><span>(8)</span></li>
                            <li><a href="#"><i class="fa fa-angle-right"></i>GREY</a><span>(4)</span></li>
                            <li><a href="#"><i class="fa fa-angle-right"></i>RED</a><span>(8)</span></li>
                            <li><a href="#"><i class="fa fa-angle-right"></i>WHITE</a><span>(6)</span></li>
                        </ul>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="popular_tag_area">
                            <div class="popular_items">
                                <h2>BESTSELLERS</h2>
                            </div>
                        </div>
                        <div class="clothing_carousel_list">
                            <div class="single_clothing_product">
                                <div class="clothing_item">
                                    <img src="img/bestseller/4.jpg" alt="" />
                                    <div class="product_clothing_details">
                                        <h2><a href="#">Nam ullamcorper vive</a></h2>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <p>$123.00</p>
                                    </div>
                                </div>
                                <div class="clothing_item">
                                    <img src="img/bestseller/6_1.jpg" alt="" />
                                    <div class="product_clothing_details">
                                        <h2><a href="#">Nam ullamcorper vive</a></h2>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <p>$123.00</p>
                                    </div>
                                </div>
                                <div class="clothing_item">
                                    <img src="img/bestseller/8.jpg" alt="" />
                                    <div class="product_clothing_details">
                                        <h2><a href="#">Nam ullamcorper vive</a></h2>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <p>$123.00</p>
                                    </div>
                                </div>
                            </div>
                            <div class="single_clothing_product">
                                <div class="clothing_item">
                                    <img src="img/bestseller/10.jpg" alt="" />
                                    <div class="product_clothing_details">
                                        <h2><a href="#">Nam ullamcorper vive</a></h2>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <p>$123.00</p>
                                    </div>
                                </div>
                                <div class="clothing_item">
                                    <img src="img/bestseller/11_1.jpg" alt="" />
                                    <div class="product_clothing_details">
                                        <h2><a href="#">Nam ullamcorper vive</a></h2>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <p>$123.00</p>
                                    </div>
                                </div>
                                <div class="clothing_item">
                                    <img src="img/bestseller/16.jpg" alt="" />
                                    <div class="product_clothing_details">
                                        <h2><a href="#">Nam ullamcorper vive</a></h2>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <p>$123.00</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="catagory_banner">
                            <img src="img/banner/category.html" alt="" />
                        </div>
                    </div>
                </div>
                <div class="my_tabs">
                    <ul class="tab_style">
                        <li class="active"><a data-toggle="tab" href="#tab1"><span><i class="fa fa-th"></i></span></a></li>
                        <li><a data-toggle="tab" href="#tab2"><span><i class="fa fa-th-list"></i></span></a></li>
                    </ul>
                    <div class="limiter">
                        <label>Show</label>
                        <select>
                            <option value="">9</option>
                            <option value="" selected="selected">12</option>
                            <option value="">24</option>
                            <option value="">36</option>
                        </select> per page
                    </div>
                    <div class="sort-by">
                        <label>Sort By</label>
                        <select>
                            <option value="" selected="selected">Position</option>
                            <option value="">Name</option>
                            <option value="">Price</option>
                        </select>
                        <a href="#"><i class="fa fa-long-arrow-up"></i></a>
                    </div>
                    <div class="tab-content tab_content_style">
                        <div id="tab1" class="tab-pane fade in active">
                            <div class="row">
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                    <div class="product_list">
                                        <div class="single_product repomsive_768">
                                            <a href="detail_product.php"><img src="img/feature-image/4.jpg" alt="" /></a>
                                            <div class="product_details">
                                                <h2>Lorem ipsum dolor</h2>
                                                <p><span class="regular_price">$170.00</span> <span class="popular_price">$150.00</span></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                    <div class="product_list">
                                        <div class="single_product repomsive_768">
                                            <a href="detail_product.php"><img src="img/feature-image/6_1.jpg" alt="" /></a>
                                            <div class="product_details">
                                                <h2>Lorem ipsum dolor</h2>
                                                <p><span class="regular_price">$170.00</span> <span class="popular_price">$150.00</span></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                    <div class="product_list">
                                        <div class="single_product repomsive_768">
                                            <a href="detail_product.php"><img src="img/feature-image/8.jpg" alt="" /></a>
                                            <div class="product_details">
                                                <h2>Lorem ipsum dolor</h2>
                                                <p><span class="regular_price">$170.00</span> <span class="popular_price">$150.00</span></p>
                                            </div>
                                            <div class="sale_product">
                                                <h5>Sale</h5>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                    <div class="product_list">
                                        <div class="single_product repomsive_768">
                                            <a href="detail_product.php"><img src="img/feature-image/23.jpg" alt="" /></a>
                                            <div class="product_details">
                                                <h2>Lorem ipsum dolor</h2>
                                                <p><span class="regular_price">$170.00</span> <span class="popular_price">$150.00</span></p>
                                            </div>
                                            <div class="sale_product">
                                                <h5>Sale</h5>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                    <div class="product_list">
                                        <div class="single_product repomsive_768">
                                            <a href="detail_product.php"><img src="img/feature-image/24.jpg" alt="" /></a>
                                            <div class="product_details">
                                                <h2>Lorem ipsum dolor</h2>
                                                <p><span class="regular_price">$170.00</span> <span class="popular_price">$150.00</span></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                    <div class="product_list">
                                        <div class="single_product repomsive_768">
                                            <a href="detail_product.php"><img src="img/feature-image/10.jpg" alt="" /></a>
                                            <div class="product_details">
                                                <h2>Lorem ipsum dolor</h2>
                                                <p><span class="regular_price">$170.00</span> <span class="popular_price">$150.00</span></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                    <div class="product_list">
                                        <div class="single_product repomsive_768">
                                            <a href="detail_product.php"><img src="img/product/12_1.jpg" alt="" /></a>
                                            <div class="product_details">
                                                <h2>Lorem ipsum dolor</h2>
                                                <p><span class="regular_price">$170.00</span> <span class="popular_price">$150.00</span></p>
                                            </div>
                                            <div class="sale_product">
                                                <h5>Sale</h5>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                    <div class="product_list">
                                        <div class="single_product repomsive_768">
                                            <a href="detail_product.php"><img src="img/product/18_1.jpg" alt="" /></a>
                                            <div class="product_details">
                                                <h2>Lorem ipsum dolor</h2>
                                                <p><span class="regular_price">$170.00</span> <span class="popular_price">$150.00</span></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                    <div class="product_list">
                                        <div class="single_product repomsive_768">
                                            <a href="detail_product.php"><img src="img/product/19_1.jpg" alt="" /></a>
                                            <div class="product_details">
                                                <h2>Lorem ipsum dolor</h2>
                                                <p><span class="regular_price">$170.00</span> <span class="popular_price">$150.00</span></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                    <div class="product_list">
                                        <div class="single_product repomsive_768">
                                            <a href="detail_product.php"><img src="img/product/12_1.jpg" alt="" /></a>
                                            <div class="product_details">
                                                <h2>Lorem ipsum dolor</h2>
                                                <p><span class="regular_price">$170.00</span> <span class="popular_price">$150.00</span></p>
                                            </div>
                                            <div class="sale_product">
                                                <h5>Sale</h5>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                    <div class="product_list">
                                        <div class="single_product repomsive_768">
                                            <a href="detail_product.php"><img src="img/product/18_1.jpg" alt="" /></a>
                                            <div class="product_details">
                                                <h2>Lorem ipsum dolor</h2>
                                                <p><span class="regular_price">$170.00</span> <span class="popular_price">$150.00</span></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                    <div class="product_list">
                                        <div class="single_product repomsive_768">
                                            <a href="detail_product.php"><img src="img/product/19_1.jpg" alt="" /></a>
                                            <div class="product_details">
                                                <h2>Lorem ipsum dolor</h2>
                                                <p><span class="regular_price">$170.00</span> <span class="popular_price">$150.00</span></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="blog_pagination">
                                        <h2>Page:</h2>
                                        <ul class="pagination_list">
                                            <li class="active">1</li>
                                            <li><a href="#">2</a></li>
                                            <li><a href="#">3</a></li>
                                            <li><a href="#"><img src="img/arrow/pager_arrow_right.gif" alt="" /></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="tab2" class="tab-pane fade">
                            <div class="row">
                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                    <div class="product_blog_image">
                                        <div class="product_blog_image">
                                            <a href="detail_product.php"><img src="img/product/1.jpg" alt="" /></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                                    <div class="blog_product_details">
                                        <h2 class="blog_heading"><a href="#">Lorem ipsum dolor</a></h2>
                                        <div class="product_rating">
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                        </div>
                                        <div class="product_rating">
                                            <a href="#">1 Review(s) <span>I</span></a>
                                            <a href="#"> Add Your Review</a>
                                        </div>
                                        <div class="pricing_rate">
                                            <p class="rating_dollor"><span class="rating_value_one">$170.00</span> <span class="rating_value_two">$150.00</span></p>
                                            <p class="blog_texts">Nunc facilisis sagittis ullamcorper. Proin lectus ipsum, gravida et mattis vulputate, tristique ut lectus. Sed et lorem nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Aenean eleifend laoreet congue. Vivamus adipiscing nisl ut dolor dignissim semper. Nulla luctus malesuada tincidunt. Class aptent taciti sociosqu ad litora torquent per conubia nostra, p <a class="learn_more" href="#">Learn More</a></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                    <div class="product_blog_image">
                                        <div class="product_blog_image">
                                            <a href="detail_product.php"><img src="img/product/4.jpg" alt="" /></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                                    <div class="blog_product_details">
                                        <h2 class="blog_heading"><a href="#">Lorem ipsum dolor</a></h2>
                                        <div class="product_rating">
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                        </div>
                                        <div class="product_rating">
                                            <a href="#">1 Review(s) <span>I</span></a>
                                            <a href="#"> Add Your Review</a>
                                        </div>
                                        <div class="pricing_rate">
                                            <p class="rating_dollor"><span class="rating_value_one">$170.00</span> <span class="rating_value_two">$150.00</span></p>
                                            <p class="blog_texts">Nunc facilisis sagittis ullamcorper. Proin lectus ipsum, gravida et mattis vulputate, tristique ut lectus. Sed et lorem nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Aenean eleifend laoreet congue. Vivamus adipiscing nisl ut dolor dignissim semper. Nulla luctus malesuada tincidunt. Class aptent taciti sociosqu ad litora torquent per conubia nostra, p <a class="learn_more" href="#">Learn More</a></p>
                                        </div>
                                        <div class="product_blog_button">
                                            <div class="cart_blog_details">
                                                <a href="#" target="blank">Add to cart</a>
                                            </div>
                                            <div class="cart_blog_details">
                                                <a href="#" target="expand"><i class="fa fa-expand"></i></a>
                                            </div>
                                            <div class="cart_blog_details">
                                                <a href="#" target="heart"><i class="fa fa-heart-o"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                    <div class="product_blog_image">
                                        <div class="product_blog_image">
                                            <a href="detail_product.php"><img src="img/product/6_1.jpg" alt="" /></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                                    <div class="blog_product_details">
                                        <h2 class="blog_heading"><a href="#">Lorem ipsum dolor</a></h2>
                                        <div class="product_rating">
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                        </div>
                                        <div class="product_rating">
                                            <a href="#">1 Review(s) <span>I</span></a>
                                            <a href="#"> Add Your Review</a>
                                        </div>
                                        <div class="pricing_rate">
                                            <p class="rating_dollor"><span class="rating_value_one">$170.00</span> <span class="rating_value_two">$150.00</span></p>
                                            <p class="blog_texts">Nunc facilisis sagittis ullamcorper. Proin lectus ipsum, gravida et mattis vulputate, tristique ut lectus. Sed et lorem nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Aenean eleifend laoreet congue. Vivamus adipiscing nisl ut dolor dignissim semper. Nulla luctus malesuada tincidunt. Class aptent taciti sociosqu ad litora torquent per conubia nostra, p <a class="learn_more" href="#">Learn More</a></p>
                                        </div>
                                        <div class="product_blog_button">
                                            <div class="cart_blog_details">
                                                <a href="#" target="blank">Add to cart</a>
                                            </div>
                                            <div class="cart_blog_details">
                                                <a href="#" target="expand"><i class="fa fa-expand"></i></a>
                                            </div>
                                            <div class="cart_blog_details">
                                                <a href="#" target="heart"><i class="fa fa-heart-o"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                    <div class="product_blog_image">
                                        <div class="product_blog_image">
                                            <a href="detail_product.php"><img src="img/product/12_1.jpg" alt="" /></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                                    <div class="blog_product_details">
                                        <h2 class="blog_heading"><a href="#">Lorem ipsum dolor</a></h2>
                                        <div class="product_rating">
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                        </div>
                                        <div class="product_rating">
                                            <a href="#">1 Review(s) <span>I</span></a>
                                            <a href="#"> Add Your Review</a>
                                        </div>
                                        <div class="pricing_rate">
                                            <p class="rating_dollor"><span class="rating_value_one">$170.00</span> <span class="rating_value_two">$150.00</span></p>
                                            <p class="blog_texts">Nunc facilisis sagittis ullamcorper. Proin lectus ipsum, gravida et mattis vulputate, tristique ut lectus. Sed et lorem nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Aenean eleifend laoreet congue. Vivamus adipiscing nisl ut dolor dignissim semper. Nulla luctus malesuada tincidunt. Class aptent taciti sociosqu ad litora torquent per conubia nostra, p <a class="learn_more" href="#">Learn More</a></p>
                                        </div>
                                        <div class="product_blog_button">
                                            <div class="cart_blog_details">
                                                <a href="#" target="blank">Add to cart</a>
                                            </div>
                                            <div class="cart_blog_details">
                                                <a href="#" target="expand"><i class="fa fa-expand"></i></a>
                                            </div>
                                            <div class="cart_blog_details">
                                                <a href="#" target="heart"><i class="fa fa-heart-o"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                    <div class="product_blog_image">
                                        <div class="product_blog_image">
                                            <a href="detail_product.php"><img src="img/product/18_1.jpg" alt="" /></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                                    <div class="blog_product_details">
                                        <h2 class="blog_heading"><a href="#">Lorem ipsum dolor</a></h2>
                                        <div class="product_rating">
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                        </div>
                                        <div class="product_rating">
                                            <a href="#">1 Review(s) <span>I</span></a>
                                            <a href="#"> Add Your Review</a>
                                        </div>
                                        <div class="pricing_rate">
                                            <p class="rating_dollor"><span class="rating_value_one">$170.00</span> <span class="rating_value_two">$150.00</span></p>
                                            <p class="blog_texts">Nunc facilisis sagittis ullamcorper. Proin lectus ipsum, gravida et mattis vulputate, tristique ut lectus. Sed et lorem nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Aenean eleifend laoreet congue. Vivamus adipiscing nisl ut dolor dignissim semper. Nulla luctus malesuada tincidunt. Class aptent taciti sociosqu ad litora torquent per conubia nostra, p <a class="learn_more" href="#">Learn More</a></p>
                                        </div>
                                        <div class="product_blog_button">
                                            <div class="cart_blog_details">
                                                <a href="#" target="blank">Add to cart</a>
                                            </div>
                                            <div class="cart_blog_details">
                                                <a href="#" target="expand"><i class="fa fa-expand"></i></a>
                                            </div>
                                            <div class="cart_blog_details">
                                                <a href="#" target="heart"><i class="fa fa-heart-o"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                    <div class="product_blog_image">
                                        <div class="product_blog_image">
                                            <a href="detail_product.php"><img src="img/product/13_4.jpg" alt="" /></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                                    <div class="blog_product_details">
                                        <h2 class="blog_heading"><a href="#">Lorem ipsum dolor</a></h2>
                                        <div class="product_rating">
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                        </div>
                                        <div class="product_rating">
                                            <a href="#">1 Review(s) <span>I</span></a>
                                            <a href="#"> Add Your Review</a>
                                        </div>
                                        <div class="pricing_rate">
                                            <p class="rating_dollor"><span class="rating_value_one">$170.00</span> <span class="rating_value_two">$150.00</span></p>
                                            <p class="blog_texts">Nunc facilisis sagittis ullamcorper. Proin lectus ipsum, gravida et mattis vulputate, tristique ut lectus. Sed et lorem nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Aenean eleifend laoreet congue. Vivamus adipiscing nisl ut dolor dignissim semper. Nulla luctus malesuada tincidunt. Class aptent taciti sociosqu ad litora torquent per conubia nostra, p <a class="learn_more" href="#">Learn More</a></p>
                                        </div>
                                        <div class="product_blog_button">
                                            <div class="cart_blog_details">
                                                <a href="#" target="blank">Add to cart</a>
                                            </div>
                                            <div class="cart_blog_details">
                                                <a href="#" target="expand"><i class="fa fa-expand"></i></a>
                                            </div>
                                            <div class="cart_blog_details">
                                                <a href="#" target="heart"><i class="fa fa-heart-o"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                    <div class="product_blog_image">
                                        <div class="product_blog_image">
                                            <a href="detail_product.php"><img src="img/product/14_3.jpg" alt="" /></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                                    <div class="blog_product_details">
                                        <h2 class="blog_heading"><a href="#">Lorem ipsum dolor</a></h2>
                                        <div class="product_rating">
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                        </div>
                                        <div class="product_rating">
                                            <a href="#">1 Review(s) <span>I</span></a>
                                            <a href="#"> Add Your Review</a>
                                        </div>
                                        <div class="pricing_rate">
                                            <p class="rating_dollor"><span class="rating_value_one">$170.00</span> <span class="rating_value_two">$150.00</span></p>
                                            <p class="blog_texts">Nunc facilisis sagittis ullamcorper. Proin lectus ipsum, gravida et mattis vulputate, tristique ut lectus. Sed et lorem nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Aenean eleifend laoreet congue. Vivamus adipiscing nisl ut dolor dignissim semper. Nulla luctus malesuada tincidunt. Class aptent taciti sociosqu ad litora torquent per conubia nostra, p <a class="learn_more" href="#">Learn More</a></p>
                                        </div>
                                        <div class="product_blog_button">
                                            <div class="cart_blog_details">
                                                <a href="#" target="blank">Add to cart</a>
                                            </div>
                                            <div class="cart_blog_details">
                                                <a href="#" target="expand"><i class="fa fa-expand"></i></a>
                                            </div>
                                            <div class="cart_blog_details">
                                                <a href="#" target="heart"><i class="fa fa-heart-o"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                    <div class="product_blog_image">
                                        <div class="product_blog_image">
                                            <a href="detail_product.php"><img src="img/product/16.jpg" alt="" /></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                                    <div class="blog_product_details">
                                        <h2 class="blog_heading"><a href="#">Lorem ipsum dolor</a></h2>
                                        <div class="product_rating">
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                        </div>
                                        <div class="product_rating">
                                            <a href="#">1 Review(s) <span>I</span></a>
                                            <a href="#"> Add Your Review</a>
                                        </div>
                                        <div class="pricing_rate">
                                            <p class="rating_dollor"><span class="rating_value_one">$170.00</span> <span class="rating_value_two">$150.00</span></p>
                                            <p class="blog_texts">Nunc facilisis sagittis ullamcorper. Proin lectus ipsum, gravida et mattis vulputate, tristique ut lectus. Sed et lorem nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Aenean eleifend laoreet congue. Vivamus adipiscing nisl ut dolor dignissim semper. Nulla luctus malesuada tincidunt. Class aptent taciti sociosqu ad litora torquent per conubia nostra, p <a class="learn_more" href="#">Learn More</a></p>
                                        </div>
                                        <div class="product_blog_button">
                                            <div class="cart_blog_details">
                                                <a href="#" target="blank">Add to cart</a>
                                            </div>
                                            <div class="cart_blog_details">
                                                <a href="#" target="expand"><i class="fa fa-expand"></i></a>
                                            </div>
                                            <div class="cart_blog_details">
                                                <a href="#" target="heart"><i class="fa fa-heart-o"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                    <div class="product_blog_image">
                                        <div class="product_blog_image">
                                            <a href="detail_product.php"><img src="img/product/17_2_1_1.jpg" alt="" /></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                                    <div class="blog_product_details">
                                        <h2 class="blog_heading"><a href="#">Lorem ipsum dolor</a></h2>
                                        <div class="product_rating">
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                        </div>
                                        <div class="product_rating">
                                            <a href="#">1 Review(s) <span>I</span></a>
                                            <a href="#"> Add Your Review</a>
                                        </div>
                                        <div class="pricing_rate">
                                            <p class="rating_dollor"><span class="rating_value_one">$170.00</span> <span class="rating_value_two">$150.00</span></p>
                                            <p class="blog_texts">Nunc facilisis sagittis ullamcorper. Proin lectus ipsum, gravida et mattis vulputate, tristique ut lectus. Sed et lorem nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Aenean eleifend laoreet congue. Vivamus adipiscing nisl ut dolor dignissim semper. Nulla luctus malesuada tincidunt. Class aptent taciti sociosqu ad litora torquent per conubia nostra, p <a class="learn_more" href="#">Learn More</a></p>
                                        </div>
                                        <div class="product_blog_button">
                                            <div class="cart_blog_details">
                                                <a href="#" target="blank">Add to cart</a>
                                            </div>
                                            <div class="cart_blog_details">
                                                <a href="#" target="expand"><i class="fa fa-expand"></i></a>
                                            </div>
                                            <div class="cart_blog_details">
                                                <a href="#" target="heart"><i class="fa fa-heart-o"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="blog_pagination">
                                        <h2>Page:</h2>
                                        <ul class="pagination_list">
                                            <li class="active">1</li>
                                            <li><a href="#">2</a></li>
                                            <li><a href="#">3</a></li>
                                            <li><a href="#"><img src="img/arrow/pager_arrow_right.gif" alt="" /></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--End clothing product area -->

<?php
include('footer.php')
?>