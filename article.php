<?php
$page = 'article';
include('header.php')
?>

<div class="blog_post_area" style="margin-top: 50px; margin-bottom: 50px;">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="feature_text">
                    <h4>Article</h4>
                </div>
            </div>
        </div>
        <div class="row">
                <div class="col-lg-4" style="margin-top:20px">
                    <div class="single_blog" style="border-radius:10px;">
                        <a href="articleDetail.php"><img src="https://images.unsplash.com/photo-1489987707025-afc232f7ea0f?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1050&q=80" style="border-radius:10px;" alt="" /></a>
                        <div class="blog_details">
                            <a href="articleDetail.php">William Eto</a>
                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam...</p>
                            <h3>18 Aug 2015</h3>
                            <a href="articleDetail.php">
                                <div class="read_more">
                                    <i class="fa fa-angle-right"></i>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4" style="margin-top:20px">
                    <div class="single_blog" style="border-radius:10px;">
                        <a href="articleDetail.php"><img src="https://images.unsplash.com/photo-1551488831-00ddcb6c6bd3?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1050&q=80" style="border-radius:10px;" alt="" /></a>
                        <div class="blog_details">
                            <a href="articleDetail.php">William Eto</a>
                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam...</p>
                            <h3>18 Aug 2015</h3>
                            <a href="articleDetail.php">
                                <div class="read_more">
                                    <i class="fa fa-angle-right"></i>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4" style="margin-top:20px">
                    <div class="single_blog" style="border-radius:10px;">
                        <a href="articleDetail.php"><img src="https://images.unsplash.com/photo-1564584217132-2271feaeb3c5?ixlib=rb-1.2.1&ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&auto=format&fit=crop&w=1050&q=80" style="border-radius:10px;" alt="" /></a>
                        <div class="blog_details">
                            <a href="articleDetail.php">William Eto</a>
                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam...</p>
                            <h3>18 Aug 2015</h3>
                            <a href="articleDetail.php">
                                <div class="read_more">
                                    <i class="fa fa-angle-right"></i>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4" style="margin-top:20px">
                    <div class="single_blog" style="border-radius:10px;">
                        <a href="articleDetail.php"><img src="https://images.unsplash.com/photo-1523381210434-271e8be1f52b?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1050&q=80" style="border-radius:10px;" alt="" /></a>
                        <div class="blog_details">
                            <a href="articleDetail.php">William Eto</a>
                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam...</p>
                            <h3>18 Aug 2015</h3>
                            <a href="articleDetail.php">
                                <div class="read_more">
                                    <i class="fa fa-angle-right"></i>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4" style="margin-top:20px">
                    <div class="single_blog" style="border-radius:10px;">
                        <a href="articleDetail.php"><img src="https://images.unsplash.com/photo-1523199455310-87b16c0eed11?ixlib=rb-1.2.1&ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&auto=format&fit=crop&w=1050&q=80" style="border-radius:10px;" alt="" /></a>
                        <div class="blog_details">
                            <a href="articleDetail.php">William Eto</a>
                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam...</p>
                            <h3>18 Aug 2015</h3>
                            <a href="articleDetail.php">
                                <div class="read_more">
                                    <i class="fa fa-angle-right"></i>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4" style="margin-top:20px">
                    <div class="single_blog" style="border-radius:10px;">
                        <a href="articleDetail.php"><img src="https://images.unsplash.com/photo-1578682443756-573e32709f94?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1050&q=80" style="border-radius:10px;" alt="" /></a>
                        <div class="blog_details">
                            <a href="articleDetail.php">William Eto</a>
                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam...</p>
                            <h3>18 Aug 2015</h3>
                            <a href="articleDetail.php">
                                <div class="read_more">
                                    <i class="fa fa-angle-right"></i>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
        </div>
    </div>
</div>


<?php
include('footer.php')
?>