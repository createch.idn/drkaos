<!doctype html>
<html class="no-js" lang="en">

<head>
    <!-- Basic page needs
        ============================================ -->
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>DR.KAOS</title>
    <meta name="description" content="">
    <!-- Mobile specific metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- FONTS -->
    <link href='https://fonts.googleapis.com/css?family=Josefin+Sans:400,600italic,300italic,700' rel='stylesheet' type='text/css'>
    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="img/favicon.png">
    <!-- font awesome -->
    <link rel="stylesheet" href="css/all.min.css">
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <!-- carousel CSS -->
    <link rel="stylesheet" href="css/owl.carousel.css">
    <!-- carousel Theme CSS -->
    <link rel="stylesheet" href="css/owl.my_theme.css">
    <!-- carousel transitions CSS -->
    <link rel="stylesheet" href="css/owl.transitions.css">
    <!-- nivo slider slider css -->
    <link rel="stylesheet" href="css/nivo-slider.css">
    <!-- animate css -->
    <link rel="stylesheet" href="css/animate.css">
    <!-- Price jquery-ui  -->
    <link rel="stylesheet" href="css/jquery-ui.css">
    <!-- fancy-box theme -->
    <link rel="stylesheet" href="fancy-box/jquery.fancybox.css">
    <!-- normalizer -->
    <link rel="stylesheet" href="css/normalize.css">
    <!-- bootstrap -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <!-- Mobile menu css -->
    <link rel="stylesheet" href="css/meanmenu.min.css">
    <!-- main -->
    <link rel="stylesheet" href="css/main.css">
    <!-- style -->
    <link rel="stylesheet" href="style.css">
    <!-- Responsive css -->
    <link rel="stylesheet" href="css/responsive.css">
    <!-- modernizr JS -->
    <script src="js/vendor/modernizr-2.8.3.min.js"></script>
    <!-- Leaflet -->
    <link rel="stylesheet" href="leaflet/leaflet.css" type="text/css">
    <script src="leaflet/leaflet.js" type="text/javascript"></script>
</head>

<body>
    <!--Start Header Top area -->
    <div class="header_area_top">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <!--Start Logo area -->
                    <div class="logo">
                        <a href="index.php">
                            <img src="img/logo/logo.png" width="200px" alt="logo" />
                        </a>
                    </div>
                    <!--End Logo area -->
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                </div>
            </div>
        </div>
    </div>
    <!--End Header Top area -->
    <!--Start Main Menu area -->
    <div class="header_botttom_area">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <!--Start desktop menu area -->
                    <div class="main_menu">
                        <ul id="nav_menu" class="active_cl">
                            <li><a <?php if ($page == 'home') echo 'style="color: #e0001b;"'?> href="index.php">Home</a></li>
                            <li><a <?php if ($page == 'product') echo 'style="color: #e0001b;"'?> href="product.php">Product</a></li>
                            <li><a <?php if ($page == 'about') echo 'style="color: #e0001b;"'?> href="about.php">About Us</a></li>
                            <li><a <?php if ($page == 'article') echo 'style="color: #e0001b;"'?> href="article.php">Article</a></li>
                        </ul>
                    </div>
                    <!--End desktop menu area -->
                </div>
            </div>
        </div>
        <!--start Mobile Menu area -->
        <div class="mobile-menu-area">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="mobile-menu">
                            <nav id="dropdown">
                                <ul>
                                    <li><a href="index.php">Home</a></li>
                                    <li><a href="product.php">Product</a></li>
                                    <li><a href="about.php">About Us</a></li>
                                    <li><a href="article.php">Article</a></li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--End Mobile Menu area -->
    </div>
    <!--End Main Menu area -->