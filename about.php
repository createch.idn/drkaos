<!-- <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous" /> -->
<style>
    .card {
        background: #fff;
        box-shadow: 5px 5px 30px rgba(34, 35, 58, 0.2);
        display: flex;
        flex-direction: row;
        border-radius: 5px;
        position: relative;
    }

    .card h2 {
        margin: 0;
        padding: 0 1rem;
    }

    .card .title {
        padding: 1rem;
        text-align: right;
        color: #e0001b;
        font-weight: bold;
        font-size: 12px;
    }

    .card .desc {
        padding: 0.5rem 1rem;
        font-size: 12px;
    }

    .card .actions {
        display: grid;
        grid-template-columns: repeat(3, 1fr);
        align-items: center;
        padding: 0.5rem 1rem;
    }

    .card svg {
        width: 85px;
        height: 85px;
        margin: 0 auto;
    }

    .img-avatar {
        width: 80px;
        height: 80px;
        position: absolute;
        border-radius: 50%;
        border: 6px solid white;
        background-image: linear-gradient(-60deg, #16a085 0%, #f4d03f 100%);
        top: 15px;
        left: 85px;
    }

    .card-text {
        display: grid;
        grid-template-columns: 1fr 2fr;
    }

    .title-total {
        padding: 2.5em 1.5em 1.5em 1.5em;
    }

    path {
        fill: white;
    }

    .img-portada {
        width: 100%;
    }

    .portada {
        width: 100%;
        height: 100%;
        border-top-left-radius: 5px;
        border-bottom-left-radius: 5px;
        background-image: url("https://m.media-amazon.com/images/S/aplus-media/vc/cab6b08a-dd8f-4534-b845-e33489e91240._CR75,0,300,300_PT0_SX300__.jpg");
        background-position: bottom center;
        background-size: cover;
    }

    button {
        border: none;
        background: none;
        font-size: 50px;
        color: #8bc34a;
        cursor: pointer;
        transition: .5s;
    }

    button:hover {
        color: #e0001b;
        transform: rotate(22deg)
    }
</style>

<?php
$page = 'about';
include('header.php')
?>

<!--Start about hope area -->
<div class="about_hope_area">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <div class="about_hope_text">
                    <h1>WELCOME TO <strong>HOPE</strong></h1>
                    <p>Lorem ipsum scelerisque molestie id molestie magna ante etiam sollicitudin tempus consectetur conubia, urna eros nunc curabitur viverra rutrum tortor luctus torquent mollis est dictum euismod</p>
                    <ul id="about_hope_details">
                        <li><i class="fa fa-angle-right"></i> We love products that work perfectly and look beautiful.</li>
                        <li><i class="fa fa-angle-right"></i> We create base on a deeply analysis of your project.</li>
                        <li><i class="fa fa-angle-right"></i> We are create design with really high quality standards.</li>
                    </ul>
                    <div class="about_read_more">
                        <a href="#">Learn More</a>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <div class="about_hope_image">
                    <img src="https://images.unsplash.com/photo-1441984904996-e0b6ba687e04?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1050&q=80" alt="" />
                </div>
            </div>
        </div>
    </div>
</div>
<!--End about hope area -->

<div class="row" style="margin-top: 100px;">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="feature_text">
            <h4>Our Location</h4>
        </div>
    </div>
</div>

<!--Start google map area -->
<div class="google_map_area" style="margin-bottom: 30px;">
    <div class="container">
        <div class="row" style="margin-bottom: 30px;">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div id="map" style="width:100%;height:433px;"></div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12" style="margin-bottom:30px;">
                <div class="card">
                    <div class="card-text">
                        <div class="portada">
                        </div>
                        <div class="title-total">
                            <div class="title">Sidoarjo</div>
                            <h2>Morgan Sweeney</h2>

                            <div class="desc">Morgan has collected ants since they were six years old and now has many dozen ants but none in their pants.</div>
                            <div class="row">
                                <button class="col-md-3"><i class="fab fa-whatsapp"></i></button>
                                <button class="col-md-3"><i class="far fa-envelope"></i></button>
                                <button class="col-md-3"><i class="fab fa-instagram"></i></button>
                                <button class="col-md-3"><i class="fas fa-map-marked-alt"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12" style="margin-bottom:30px;">
                <div class="card">
                    <div class="card-text">
                        <div class="portada">
                        </div>
                        <div class="title-total">
                            <div class="title">Bali</div>
                            <h2>Morgan Sweeney</h2>

                            <div class="desc">Morgan has collected ants since they were six years old and now has many dozen ants but none in their pants.</div>
                            <div class="row">
                                <button class="col-md-3"><i class="fab fa-whatsapp"></i></button>
                                <button class="col-md-3"><i class="far fa-envelope"></i></button>
                                <button class="col-md-3"><i class="fab fa-instagram"></i></button>
                                <button class="col-md-3"><i class="fas fa-map-marked-alt"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
<!--End google map area -->

<script type="text/javascript">
    var map = L.map('map', {
        attributionControl: false
    }).setView([-2.159343, 117.875904], 5);

    var mapBox = L.tileLayer('https://api.mapbox.com/styles/v1/yunusdwis/ckn7h8rk51ao317mesiuyysh5/tiles/256/{z}/{x}/{y}@2x?access_token=pk.eyJ1IjoieXVudXNkd2lzIiwiYSI6ImNrajZ6OHhnejRoYWsydnJ4a2R1cmdycTIifQ.bIIzQNoaKPMGEln-maBWfQ');
    var googleStreets = L.tileLayer('http://{s}.google.com/vt/lyrs=m&x={x}&y={y}&z={z}', {
        maxZoom: 20,
        subdomains: ['mt0', 'mt1', 'mt2', 'mt3']
    });
    var googleHybrid = L.tileLayer('http://{s}.google.com/vt/lyrs=s,h&x={x}&y={y}&z={z}', {
        maxZoom: 20,
        subdomains: ['mt0', 'mt1', 'mt2', 'mt3']
    });

    var baseMaps = {
        "MapBox": mapBox,
        "Google Street": googleStreets,
        "Google Hybrid": googleHybrid
    };

    var myIcon = L.icon({
        iconUrl: 'leaflet/images/marker-icon-2x.png',
        shadowUrl: 'leaflet/images/marker-shadow.png',
        iconSize: [50],
        iconAnchor: [30, 90],
        popupAnchor: [-3, -90],
        shadowAnchor: [13, 52]
    });
    var officePoin = L.marker([-7.311018, 112.685027], {
        icon: myIcon
    }).bindPopup("Createch.id Office").addTo(map);

    L.control.layers(baseMaps).addTo(map);
    mapBox.addTo(map);
</script>

<?php
include('footer.php')
?>