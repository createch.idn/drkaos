<?php
$page = 'article';
include('header.php')
?>

<div class="blog_post_area">
	<div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="feature_text">
                    <h4>Article</h4>
                </div>
            </div>
				<div class="row">
					<div class="col-lg-12">
						<div class="blog_list_area">
							<div class="single_blog_area">
								<img src="img/slider-image/1.jpg" alt="" />
							</div>
							<div class="single_blog_area">
								<img src="img/slider-image/2.jpg" alt="" />
							</div>
							<div class="single_blog_area">
								<img src="img/slider-image/3.jpg" alt="" />
							</div>
						</div>
						<div class="blog_details_area">
							<div class="blog_info_details">
								 <h2><a class="blog_info_heading" href="#">121212 dikabarkan kiamat tapi boong</a></h2>
								 <p class="blog_paragrap_style">12 December 2012</p>
								 <p class="blog_paragrap_style">Aliquam et metus pharetra, bibendum massa nec, fermentum odio. Nunc id leo ultrices, mollis ligula in, finibus tortor. Mauris eu dui ut lectus fermentum eleifend. Pellentesque faucibus sem ante, non malesuada odio varius nec. Suspendisse potenti. Proin consectetur aliquam odio nec fringilla. Sed interdum at justo in efficitur. Vivamus gravida volutpat sodales. Fusce ornare sit amet ligula condimentum sagittis.</p>
								<blockquote>
									<p>Quisque semper nunc vitae erat pellentesque, ac placerat arcu consectetur. In venenatis elit ac ultrices convallis. Duis est nisi, tincidunt ac urna sed, cursus blandit lectus. In ullamcorper sit amet ligula ut eleifend. Proin dictum tempor ligula, ac feugiat metus. Sed finibus tortor eu scelerisque scelerisque.</p>
								</blockquote>
								 <p class="blog_paragrap_style">Aenean et tempor eros, vitae sollicitudin velit. Etiam varius enim nec quam tempor, sed efficitur ex ultrices. Phasellus pretium est vel dui vestibulum condimentum. Aenean nec suscipit nibh. Phasellus nec lacus id arcu facilisis elementum. Curabitur lobortis, elit ut elementum congue, erat ex bibendum odio, nec iaculis lacus sem non lorem. Duis suscipit metus ante, sed convallis quam posuere quis. Ut tincidunt eleifend odio, ac fringilla mi vehicula nec. Nunc vitae lacus eget lectus imperdiet tempus sed in dui. Nam molestie magna at risus consectetur, placerat suscipit justo dignissim. Sed vitae fringilla enim, nec ullamcorper arcu.</p>
								 <p class="blog_paragrap_style">Suspendisse turpis ipsum, tempus in nulla eu, posuere pharetra nibh. In dignissim vitae lorem non mollis. Praesent pretium tellus in tortor viverra condimentum. Nullam dignissim facilisis nisl, accumsan placerat justo ultricies vel. Vivamus finibus mi a neque pretium, ut convallis dui lacinia. Morbi a rutrum velit. Curabitur sagittis quam quis consectetur mattis. Aenean sit amet quam vel turpis interdum sagittis et eget neque. Nunc ante quam, luctus et neque a, interdum iaculis metus. Aliquam vel ante mattis, placerat orci id, vehicula quam. Suspendisse quis eros cursus, viverra urna sed, commodo mauris. Cras diam arcu, fringilla a sem condimentum, viverra facilisis nunc. Curabitur vitae orci id nulla maximus maximus. Nunc pulvinar sollicitudin molestie.</p>
							</div>
						</div>
					</div>
				</div>
			</div>
	</div>
    <div class="row">
            <div class="col-lg-12">
                <div class="feature_text">
                    <h4>Other Article</h4>
                </div>
            </div>
        </div>
    <div class="row" style="margin-left:22px;margin-right:22px;margin-bottom:20px;margin-top:20px">
            <div class="carousel_blog_list">
                <div class="col-lg-3">
                    <div class="single_blog">
                        <a href="#"><img src="img/blog-post-image/cl-1.jpg" alt="" /></a>
                        <div class="blog_details">
                            <a href="#">William Eto</a>
                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam...</p>
                            <h3>18 Aug 2015</h3>
                            <a href="#">
                                <div class="read_more">
                                    <i class="fa fa-angle-right"></i>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="single_blog">
                        <a href="#"><img src="img/blog-post-image/cl-2.jpg" alt="" /></a>
                        <div class="blog_details">
                            <a href="#">William Eto</a>
                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam...</p>
                            <h3>18 Aug 2015</h3>
                            <a href="#">
                                <div class="read_more">
                                    <i class="fa fa-angle-right"></i>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="single_blog">
                        <a href="#"><img src="img/blog-post-image/cl-3.jpg" alt="" /></a>
                        <div class="blog_details">
                            <a href="#">William Eto</a>
                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam...</p>
                            <h3>18 Aug 2015</h3>
                            <a href="#">
                                <div class="read_more">
                                    <i class="fa fa-angle-right"></i>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="single_blog">
                        <a href="#"><img src="img/blog-post-image/cl-4.jpg" alt="" /></a>
                        <div class="blog_details">
                            <a href="#">William Eto</a>
                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam...</p>
                            <h3>18 Aug 2015</h3>
                            <a href="#">
                                <div class="read_more">
                                    <i class="fa fa-angle-right"></i>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="single_blog">
                        <a href="#"><img src="img/blog-post-image/cl-3.jpg" alt="" /></a>
                        <div class="blog_details">
                            <a href="#">William Eto</a>
                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam...</p>
                            <h3>18 Aug 2015</h3>
                            <a href="#">
                                <div class="read_more">
                                    <i class="fa fa-angle-right"></i>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="single_blog">
                        <a href="#"><img src="img/blog-post-image/cl-1.jpg" alt="" /></a>
                        <div class="blog_details">
                            <a href="#">William Eto</a>
                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam...</p>
                            <h3>18 Aug 2015</h3>
                            <a href="#">
                                <div class="read_more">
                                    <i class="fa fa-angle-right"></i>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</div>


<?php
include('footer.php')
?>